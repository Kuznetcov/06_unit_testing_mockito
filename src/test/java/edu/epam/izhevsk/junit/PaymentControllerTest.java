package edu.epam.izhevsk.junit;

import org.hamcrest.CustomMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.mockito.AdditionalMatchers.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class PaymentControllerTest {
  
  @Mock
  AccountService mockAccount;
  @Mock
  DepositService mokeDeposit;
  @InjectMocks
  PaymentController controller;

  
  @Before
  public void setup() throws InsufficientFundsException {
//    Вариант использования Mockito без аннотаций
//    mockAccount = mock(AccountService.class);
//    mokeDeposit = mock(DepositService.class);
//    controller = new PaymentController(mockAccount, mokeDeposit);
    
    when(mockAccount.isUserAuthenticated(100l)).thenReturn(true);
    when(mokeDeposit.deposit(lt(100l), anyLong())).thenReturn("Поздравляем!");
    when(mokeDeposit.deposit(geq(100l), anyLong())).thenThrow(InsufficientFundsException.class);
  }

  @Test(expected = SecurityException.class)
  public void testSecurityExceptionWhenUserNotAuthentificated() throws InsufficientFundsException {
    AccountService fakeAccount = mock(AccountService.class);
    DepositService fakeDeposit = mock(DepositService.class);
    PaymentController trueController = new PaymentController(fakeAccount, fakeDeposit);
    trueController.deposit(Long.valueOf(100500), Long.valueOf(404));
  }

  @Test
  public void testPaymentControllerWorkWithStub() throws InsufficientFundsException {
    controller.deposit(50l, 100l);
    verify(mokeDeposit).deposit(50l,100l);
    verify(mockAccount, atMost(1)).isUserAuthenticated(100l);
  }

  @Test(expected = InsufficientFundsException.class)
  public void testInsufficientExceptionWhenDepositAmountOverMax() throws InsufficientFundsException {
    controller.deposit(500l, 100l);
    
  }
}


// argThat(new ArgumentMatcher<Long>() {
//
// @Override
// public boolean matches(Object arg0) {
// Long val = (Long) arg0;
// return (val < 100);
// }
//
// })

